#!/usr/bin/env bash

# don't install test package
rm -r test
$PYTHON -m pip install . --no-deps -vv
